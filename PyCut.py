#!/usr/bin/env python2

# https://docs.python.org/zh-tw/3/library/argparse.html#required
# https://stackoverflow.com/questions/25513043/python-argparse-fails-to-parse-hex-formatting-to-int-type

##
# PyCut.py
#
# Version 0.2
# BIOS-CMD-Core-CE1-Team3 - Simon Yang
# (c)2020 American Megatrends, Inc. Taiwan Branch
###

import sys, argparse

def checkFile(file):
    try:
        open(file, "r")
    except IOError:
        print(file + " does not exist!")
        sys.exit(1)

# Define main method that calls other functions
def main():
    parser = argparse.ArgumentParser(prog='PyCut', description='A utility to split the binary file with specific offset and size.')
    parser.add_argument('-f', '--file',   help='Source file path',  required=True)
    parser.add_argument('-o', '--offset', help='Offset in hex/dec', required=True, type=lambda x: int(x,0))
    parser.add_argument('-s', '--size',   help='Size in hex/dec',   required=True, type=lambda x: int(x,0))
    parser.add_argument('-t', '--target', help='Target File path',  required=True)
    args = parser.parse_args()

    checkFile(args.file)

    with open(args.file, 'rb') as f:
        f.seek(0, 2)  # Seek the end
        if (args.offset + args.size) <= f.tell():
            f.seek(args.offset)
            data = f.read(args.size)
        else:
            print('The offset + size larger than source file ' + args.file)
            sys.exit()

    ff = open(args.target, mode='wb')
    ff.write(data)
    ff.close()

# Execute main() function
if __name__ == '__main__':
    main()
